import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.xml.transform.Source;
import static java.lang.Math.sqrt;
import static java.lang.Math.*;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static org.opencv.core.CvType.*;
import static org.opencv.imgproc.Imgproc.*;
import static org.opencv.core.Core.*;


public class OpticalFormAnalysis {


    public static void main(String[] args)
    {

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        String filepath = "sample1.jpg";
        Mat SourceFile = Imgcodecs.imread(filepath);

        Mat corrected = findBiggestContour(SourceFile,1,0);

        Imgcodecs.imwrite("corrected.jpg",corrected);

        float expectedWidth = (float)corrected.width()/6; // Bu 6 değeri cevap anahtarından sağlanacak şu anlık resme özel elle giriliyor
        Mat subBox = findBiggestContour(corrected,0,expectedWidth);
        Imgcodecs.imwrite("subBox.jpg",subBox);

        Mat correctedGray= corrected.clone();
        Imgproc.cvtColor(corrected,correctedGray,COLOR_BGR2GRAY);

        Imgcodecs.imwrite("corrected_gray.jpg",correctedGray);

        Mat thresh = new Mat();
        Imgproc.threshold(correctedGray,thresh,128,255,THRESH_OTSU);
        Imgproc.threshold(thresh,thresh,128,255,THRESH_BINARY_INV);

        Imgcodecs.imwrite("thresh.jpg",thresh);

        List<MatOfPoint> contoursBW = new ArrayList<>();
        List<MatOfPoint> contoursBWofQuestions = new ArrayList<>();
        Mat hierarchyBW = new Mat();
        Imgproc.findContours(thresh,contoursBW,hierarchyBW,RETR_EXTERNAL,CHAIN_APPROX_SIMPLE);

        System.out.println(contoursBW.size());
        for(int i = 0; i<contoursBW.size();i++)
        {
            Rect tempRect = boundingRect(contoursBW.get(i));
            float ratio = (float)tempRect.width/(float)tempRect.height;
            if(tempRect.width>=20 && tempRect.height>=20 && ratio>= 0.9 && ratio<= 1.1)
            {
                contoursBWofQuestions.add(contoursBW.get(i));
            }
        }

        Mat correctedWithContours = corrected.clone();

        System.out.println(contoursBWofQuestions.size());
        for(int i = 0; i< contoursBWofQuestions.size();i++)
        {
            Imgproc.drawContours(correctedWithContours, contoursBWofQuestions, i, new Scalar(0,255,0), 5);

        }

        Imgcodecs.imwrite("result-ss3c.jpg",correctedWithContours);

        List<MatOfPoint> sortedContours = sortContoursFromTopLeft(contoursBWofQuestions);


        Mat correctedWithMultiColor = corrected.clone();
        for(int i = 0; i< sortedContours.size();i++)
        {
            Imgproc.drawContours(correctedWithMultiColor, sortedContours, i, new Scalar(0,i*5,0), 5);

        }

        Imgcodecs.imwrite("result-ss3crainbow.jpg",correctedWithMultiColor);
        int k = sortedContours.size()/5;
        int [][] bubbled = new int [k][2];

        for(int i = 0; i< k; i++)
        {
            for (int j = 0 ; j<2;j++)
            {
                bubbled [i][j]=-1;
            }
        }



        for (int i = 0; i < sortedContours.size(); i++)
        {

            int j = i/5;

            Mat mask = Mat.zeros(thresh.size(), CV_8U);

            drawContours(mask, sortedContours,i, new Scalar(255),-1);

            //Rect asd = boundingRect(sortedContours.get(i));
            Mat result = new Mat();
            thresh.copyTo(result,mask);

            String filepath2 = "masks/"+i+".jpg";
            Imgcodecs.imwrite(filepath2,result);

            int total = countNonZero(result);

            if (( bubbled[j][0]==-1 && bubbled[j][1]==-1 )|| total > bubbled[j][0])
            {
                bubbled[j][0] = total;
                bubbled[j][1] = i;
            }

        }


        for(int j = 0; j<k ;j++)
        {
            System.out.print(bubbled[j][0]);
            System.out.print("\t");
            System.out.print(bubbled[j][1]);
            System.out.print("\n");
        }




        //Ekrana sonuclari yazdirma asamasi

        Mat img4print = corrected.clone();

        int []answers = new int[k];
        answers[0]=1;      //B,E,A,C,B
        answers[1]=4;
        answers[2]=1;
        answers[3]=2;
        answers[4]=1;

        int correctAnswers = 0;
        int falseAnswers = 0;
        for(int i = 0; i<k ; i++)
        {
            if((bubbled[i][1]%5)==answers[i])//Doğruysa yeşile boya
            {

                Imgproc.drawContours(img4print,sortedContours,bubbled[i][1],new Scalar (0,255,0),2);
                correctAnswers++;

            }
            else//yanlışsa kırmızıya boya, doğrusunuda yeşile boya
            {
                Imgproc.drawContours(img4print,sortedContours,bubbled[i][1],new Scalar (0,0,255),3);
                int questionIndex = bubbled[i][1]/5;
                Imgproc.drawContours(img4print,sortedContours,questionIndex*5+questionIndex,new Scalar (255,0,0),2);
                falseAnswers++;

            }
        }


        Imgcodecs.imwrite("output.jpg",img4print);

        Mat imgWithResults =img4print.clone();


        Imgproc.putText(imgWithResults,("D: "+String.valueOf(correctAnswers)),new Point (img4print.width()/2,img4print.height()-40),1,2,new Scalar(0,255,0));
        Imgproc.putText(imgWithResults,("Y: "+String.valueOf(falseAnswers)),new Point (img4print.width()/2,img4print.height()-20),1,2,new Scalar(0,0,255));



        Imgcodecs.imwrite("output2.jpg",imgWithResults);
        System.out.println("Done!");


    }
    public static List<MatOfPoint> sortContoursFromTopLeft(List<MatOfPoint> contours)
    {

        List<MatOfPoint> sortedContours = new ArrayList<>();
        while(contours.size()>0)
        {
            int next_contour_index = findTheNextContour(contours);
            sortedContours.add(contours.get(next_contour_index));
            contours.remove(next_contour_index);
        }

        return sortedContours;
    }
    public static int findTheNextContour(List<MatOfPoint> contours)
    {
        MatOfPoint nextPoint;
        int distance= 1000000000;
        int max_index=-1;
        for (int i = 0; i< contours.size();i++)
        {
            if( findCenterOfContour(contours.get(i)).x +findCenterOfContour(contours.get(i)).y*10 < distance)
            {
                max_index = i;
            }
        }


        return max_index;

    }
    public static Point findCenterOfContour(MatOfPoint contour)
    {
        float total_x = 0;
        float total_y = 0;
        for(int i = 0; i< contour.toList().size();i++)
        {
            total_x += contour.toList().get(i).x;
            total_y += contour.toList().get(i).y;

        }

        return new Point(total_x/contour.height(),total_y/contour.height());
    }
    public static Point findClosestPoint(Point referancePoint, List<Point> contourPoints)
    {

        Point closestPoint= new Point (referancePoint.x,referancePoint.y);
        double distance=1250000;
        for(int i=0;i<contourPoints.size();i++) {


            Point tempPoint1 = contourPoints.get(i);

            double tempDistance=calculateDistance(tempPoint1,referancePoint);

            if(tempDistance<distance)
            {
                closestPoint.x=tempPoint1.x;
                closestPoint.y=tempPoint1.y;
                distance = tempDistance;
            }

        }
        return closestPoint;
    }
    public static double calculateDistance(Point point1, Point point2)
    {
        return  sqrt(((point1.x-point2.x)*(point1.x-point2.x))+ ((point1.y-point2.y)*(point1.y-point2.y)));
    }
    public static Mat findBiggestContour(Mat SourceFile, int extraSpace,float expectedWidth)
    {

        Mat gray = new Mat();
        Mat blurred = new Mat();
        Mat edged = new Mat();
        List<MatOfPoint> cnts = new ArrayList<>();
        Imgproc.cvtColor(SourceFile,gray,COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(gray,blurred,new Size(5,5),0);
        Imgproc.Canny(blurred,edged,75,200);

        Imgcodecs.imwrite("result-blur.jpg",edged);

        Mat hierarchy = new Mat();


        Imgproc.findContours(edged.clone(),cnts,hierarchy,RETR_EXTERNAL,CHAIN_APPROX_SIMPLE);


        double maxVal = 0;
        int maxValIdx = 0;
        for (int contourIdx = 0; contourIdx < cnts.size(); contourIdx++)
        {
            double contourArea = Imgproc.contourArea(cnts.get(contourIdx));
            if (maxVal < contourArea)
            {
                if(expectedWidth>0)
                {
                    if( (contourArea >(expectedWidth*edged.height()*(0.7))  && ( contourArea <(expectedWidth*edged.height()*(1.5) ) )))
                    {
                        maxVal = contourArea;
                        maxValIdx = contourIdx;

                    }


                }
                else
                {
                    maxVal = contourArea;
                    maxValIdx = contourIdx;
                }

            }
        }

        Mat sourceWithContours = SourceFile.clone();
        Imgproc.drawContours(sourceWithContours, cnts, maxValIdx, new Scalar(0,255,0), 5);
        Imgcodecs.imwrite("result-ss.jpg",sourceWithContours);

        List<Point> contourPoints = cnts.get(maxValIdx).toList();

        Point tlPoint= findClosestPoint(new Point(0,0),contourPoints);
        Point trPoint=findClosestPoint(new Point (sourceWithContours.width(),0),contourPoints);
        Point brPoint=findClosestPoint(new Point(sourceWithContours.width(),sourceWithContours.height()),contourPoints);
        Point blPoint=findClosestPoint(new Point(0,sourceWithContours.height()),contourPoints);

        //calculating middle points inside contour
        Point topPoint = new Point((tlPoint.x+trPoint.x)/2,(tlPoint.y+trPoint.y)/2);
        Point bottomPoint = new Point((blPoint.x+brPoint.x)/2,(blPoint.y+brPoint.y)/2);
        Point rightPoint = new Point((trPoint.x+brPoint.x)/2,(trPoint.y+brPoint.y)/2);
        Point leftPoint = new Point((tlPoint.x+blPoint.x)/2,(tlPoint.y+blPoint.y)/2);


        if(extraSpace>0)
        {
            if(tlPoint.x-extraSpace >= 0)
            tlPoint.x= tlPoint.x-extraSpace;
            if(tlPoint.y-extraSpace >= 0)
                tlPoint.y= tlPoint.y-extraSpace;

            if(trPoint.x+extraSpace <= sourceWithContours.width())
                trPoint.x= trPoint.x+extraSpace;
            if(trPoint.y-extraSpace >= 0)
                trPoint.y= trPoint.y-extraSpace;

            if(brPoint.x+extraSpace <= sourceWithContours.width())
                brPoint.x= brPoint.x+extraSpace;
            if(brPoint.y+extraSpace <= sourceWithContours.height())
                brPoint.y= brPoint.y+extraSpace;

            if(blPoint.x-extraSpace >= 0)
                blPoint.x= blPoint.x-extraSpace;
            if(blPoint.y+extraSpace <= sourceWithContours.height())
                blPoint.y= blPoint.y+extraSpace;
        }

        double maxWdth= sqrt((leftPoint.x-rightPoint.x)*(leftPoint.x-rightPoint.x)+(leftPoint.y-rightPoint.y)*(leftPoint.y-rightPoint.y));
        double maxHeight= sqrt((bottomPoint.x-topPoint.x)*(bottomPoint.x-topPoint.x)+(bottomPoint.y-topPoint.y)*(bottomPoint.y-topPoint.y));

        MatOfPoint2f src = new MatOfPoint2f(
                tlPoint,trPoint,brPoint,blPoint
        );

        MatOfPoint2f dst = new MatOfPoint2f(

                new Point(0,0),
                new Point(maxWdth,0),
                new Point(maxWdth,maxHeight),
                new Point(0,maxHeight)
        );
        Mat pTransform = Imgproc.getPerspectiveTransform(src, dst);
        Mat corrected = new Mat ((int)maxWdth,(int)maxHeight,CV_8UC3);
        Imgproc.warpPerspective(SourceFile,corrected,pTransform,new Size (maxWdth,maxHeight));



        return corrected;
    }
}
